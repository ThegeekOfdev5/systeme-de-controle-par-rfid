import 'package:flutter/material.dart';
import 'package:flutter_hsvcolor_picker/flutter_hsvcolor_picker.dart';
import 'package:lottie/lottie.dart';
import 'package:up_pro/views/widgets/showsnackbar.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../constants/color_global.dart';

class Settings extends StatefulWidget {
  const Settings({super.key});

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  final _colorNotifier = ValueNotifier<Color>(Colors.green);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 10, left: 15, bottom: 10, right: 10),
        padding: const EdgeInsets.all(15),
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(15)),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                surfaceTintColor: Colors.white,
                elevation: 10,
                child: ListTile(
                  title: const Text("Rénitialisation de l'historique",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                          color: Colors.black)),
                  trailing: const Icon(Icons.online_prediction),
                  onTap: () async {
                    setState(() async {
                      final Uri _url = Uri.parse(
                          'https://supabase.com/dashboard/project/ttrjntfzhfivtebsgeeq/editor/29597');
                      if (!await launchUrl(_url)) {
                        ScaffoldMessenger.of(context).showSnackBar(showSnackBar(
                            "Error", "Could not launch $_url", Colors.red));
                      }
                    });
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                surfaceTintColor: Colors.white,
                elevation: 10,
                child: ListTile(
                  title: const Text("Supprimer administrateur second",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                          color: Colors.black)),
                  trailing: const Icon(Icons.online_prediction),
                  onTap: () async {
                    setState(() async {
                      final Uri _url = Uri.parse(
                          'https://supabase.com/dashboard/project/ttrjntfzhfivtebsgeeq/auth/users');
                      if (!await launchUrl(_url)) {
                        ScaffoldMessenger.of(context).showSnackBar(showSnackBar(
                            "Error", "Could not launch $_url", Colors.red));
                      }
                    });
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                surfaceTintColor: Colors.white,
                elevation: 10,
                child: ListTile(
                  leading: TextButton(
                    child: const Text("Personnaliser couleur",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                            color: Colors.black)),
                    onPressed: () {
                      setState(() {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return SimpleDialog(
                                shape: const RoundedRectangleBorder(),
                                children: [
                                  Container(
                                    height: 350,
                                    width: 400,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 12),
                                    child: ValueListenableBuilder<Color>(
                                      valueListenable: _colorNotifier,
                                      builder: (_, color, __) {
                                        return ColorPicker(
                                          color: color,
                                          onChanged: (value) {
                                            setState(() {
                                              colorBlueGlobal = value;
                                            });
                                          },
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              );
                            });
                      });
                    },
                  ),
                  title: Center(
                    child: Text("Couleur : $colorBlueGlobal",
                        style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                            color: Colors.black)),
                  ),
                  trailing: IconButton(
                      tooltip: 'Couleur par Default',
                      onPressed: () {
                        setState(() {
                          colorBlueGlobal = const Color(0xFF201658);
                        });
                      },
                      icon: const Icon(Icons.change_circle_outlined)),
                ),
              ),
            ),
            Expanded(
                child: Center(
              child: Lottie.asset("assets/lotties/search_setting.json"),
            )),
          ],
        ));
  }
}
