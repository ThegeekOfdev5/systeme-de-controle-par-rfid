import 'dart:math';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:fluid_dialog/fluid_dialog.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:up_pro/controller/controllerDataBase.dart';
import 'package:up_pro/models/personnel.dart';
import 'package:up_pro/views/administration/widgets/widget_table_person/uid_person.dart';
import 'package:up_pro/views/list_items/list_items.dart';
import 'package:vs_scrollbar/vs_scrollbar.dart';

import '../../../widgets/showsnackbar.dart';


class RowTableGestionPerson extends StatefulWidget {
  final  PersonnelModel personne;
  final String? email;
  const RowTableGestionPerson({Key? key, required this.personne, this.email})
      : super(key: key);

  @override
  State<RowTableGestionPerson> createState() => _RowTableGestionPersonState();
}

class _RowTableGestionPersonState extends State<RowTableGestionPerson> {

  bool isHovering = true;
  final supabase = Supabase.instance.client;
  final ScrollController _controller=ScrollController();
  late TextEditingController controllerTextNom;
  late TextEditingController controllerTextPrenom;
  late TextEditingController controllerTextContact;
  late TextEditingController controllerTextTitre;
  late TextEditingController controllerTextDepartement;
  late TextEditingController controllerTextEmail;
  late ControllerDataBase bdController;
  bool switchValue=false;

  void updateModifedCell()async{
    controllerTextNom.text=widget.personne.nom;
    controllerTextPrenom.text=widget.personne.prenom;
    controllerTextContact.text=widget.personne.contact;
    controllerTextTitre.text=widget.personne.titre;
    controllerTextDepartement.text=widget.personne.departement;
    controllerTextEmail.text=widget.personne.email;
  }
  @override
  void initState() {
    Get.put(ControllerDataBase());
    bdController=Get.find();
    controllerTextNom=TextEditingController();
    controllerTextPrenom=TextEditingController();
    controllerTextContact=TextEditingController();
    controllerTextTitre=TextEditingController();
    controllerTextDepartement=TextEditingController();
    controllerTextEmail=TextEditingController();
    updateModifedCell();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      mouseCursor: SystemMouseCursors.basic,
      onDoubleTap: () {
        setState(() {
          showDialog(context:context, builder:(context){
            return FluidDialog(
              rootPage: FluidDialogPage(
                  alignment: Alignment.center,
                  builder: (context) {
                    bool switched=false;
                    return Container(
                      height:120,
                      width:250,
                      padding: EdgeInsets.all(12),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Nom : ${widget.personne.nom}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17),),
                          SizedBox(height:5,),
                          Text("Prenom : ${widget.personne.prenom}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17),),
                          Row(
                            children: [
                              Text("Administrateur :",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17),),
                              SizedBox(width:7,),
                              Switch(value:widget.personne.admin!, onChanged:(value){
                              })
                            ],
                          )
                        ],
                      ),
                    );
                  }),);
          });
        });
      },
      onHover: (value) {
        setState(() {
          isHovering = value;
        });
      },
      child: Material(
        elevation:10,
        child: Container(
          padding: const EdgeInsets.only(left: 2.0),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black, width: 1.0),
            color: Color(0xFFF4F4F4),
          ),
          height: 40,
          width: 800,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              // Réf
              Container(
                height: 40,
                width: 80,
                alignment: Alignment.centerLeft,
                color: Colors.transparent,
                child: Row(
                  children: [
                    IconButton(
                      onPressed:()async{
                        updateModifedCell();
                        controllerDataBase.initialisation();
                        await update();
                      },
                      splashRadius:5,
                      icon: Icon(Icons.mode_edit_outlined, size: 20,
                        color: Colors.blue,),
                    ),
                    widget.email==widget.personne.email?
                    IconButton(
                      tooltip: "Administrateur",
                      onPressed:null,
                      splashRadius:5,
                      icon: Icon(Icons.person_remove, size: 20,
                        color: Colors.red,),
                    )
                        :IconButton(
                      tooltip:widget.personne.admin!?"Supprimer admin second":"Supprimer" ,
                      onPressed:()async{
                        await delete();
                      },
                      splashRadius:5,
                      icon: widget.personne.admin!?Icon(Icons.delete_sweep_sharp, size: 20, color: Colors.red,):Icon(Icons.delete, size: 20, color: Colors.red,),
                    ),
                  ],
                ),
              ),
              // Intitule
              Container(
                height: 40,
                width: 150,
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child: Text(
                  widget.personne.nom,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ),
              ),
              //type
              Container(
                height: 40,
                width: 250,
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child: Text(
                    widget.personne.prenom,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ),
              ),

              //unite
              Container(
                height: 40,
                width: 150,
                color: Colors.transparent,
                alignment: Alignment.centerLeft,
                child: Text(
                    widget.personne.contact,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 16),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ),
              ),
              // Réalise Annuel
              Container(
                  height: 40,
                  width: 160,
                  color: Colors.transparent,
                  alignment: Alignment.centerLeft,
                  child: Text(
                      widget.personne.titre,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 16),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                  )),
              // Réalise Mois
              Container(
                  height: 40,
                  width: 150,
                  color: Colors.transparent,
                  alignment: Alignment.centerLeft,
                  child: Text(
                      widget.personne.departement,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 16),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                  )),
              //cible
              InkWell(
                onTap: (){
                setState(() {
                  showDialog(
                      context: context,
                      builder:(context){
                        return SimpleDialog(
                          title:Text("Configuration Badge",style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold),),
                          elevation:10,
                          children: [
                            Container(
                              height:480,
                              width:330,
                              padding: EdgeInsets.only(left:5,right: 15),
                              alignment: Alignment.center,
                              child: VsScrollbar(
                                controller:_controller,
                                isAlwaysShown:true,
                                child: ListView(
                                  controller: _controller,
                                  children: [
                                   UidPersonn(personne:widget.personne)
                                  ],
                                ),
                              ),
                            )
                          ],
                        );
                      });
                });
                },
                child: Container(
                  height: 40,
                  width: 150,
                  margin: EdgeInsets.only(top:5,bottom:5),
                  decoration:BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30)
                  ),
                  alignment: Alignment.center,
                  child:Text("${widget.personne.code_empreinte}",maxLines: 4,overflow:TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.blueAccent)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  Future<void> update() async {
    showDialog(
        context:context,
        builder:(context){
          return SimpleDialog(
              title:Text("Modification"),
            contentPadding: EdgeInsets.all(10),
              shape:RoundedRectangleBorder(
                borderRadius:BorderRadius.circular(20)
              ),
            children: [
              ListTile(
                leading: Text("Nom :             ",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                title: Container(
                  height: 50,
                  width: 200,
                  child: TextFormField(
                    controller: controllerTextNom,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        fillColor:Colors.white,
                      filled: true,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 5,),
              ListTile(
                leading: Text("Prenom :         ",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                title: Container(
                  height: 50,
                  width: 200,
                  child: TextFormField(
                    controller: controllerTextPrenom,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      fillColor:Colors.white,
                      filled: true,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 5,),
              ListTile(
                leading: Text("Contact :         ",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                title: Container(
                  height: 50,
                  width: 200,
                  child: TextFormField(
                    controller: controllerTextContact,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      fillColor:Colors.white,
                      filled: true,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 5,),
              ListTile(
                leading: Text("Titre :              ",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                title: Container(
                  height: 50,
                  width: 200,
                  child: TextFormField(
                    controller: controllerTextTitre,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      fillColor:Colors.white,
                      filled: true,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 5,),
              ListTile(
                leading: Text("Departement :",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                title: Container(
                  height: 50,
                  width: 200,
                  child: TextFormField(
                    controller:controllerTextDepartement,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      fillColor:Colors.white,
                      filled: true,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 5,),
              ListTile(
                leading: Text("Email :             ",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                title: Container(
                  height: 50,
                  width: 200,
                  child: TextFormField(
                    enabled:widget.personne.admin!?false:true,
                    controller:controllerTextEmail,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      fillColor:Colors.white,
                      filled: true,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 5,),
              ListTile(
                leading: Text("Admin :           ",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                title: Container(
                  height: 50,
                  width: 200,
                  child: DropdownSearch(
                    enabled: widget.personne.code_empreinte=="XXXXXXXX"?false:true,
                  selectedItem:widget.personne.admin!?"TRUE":"FALSE",
                      onChanged:(value){
                        setState(() {
                          switchValue=value=="TRUE"?true:false;
                        });
                      },
                      items:[
                        "TRUE",
                        "FALSE"
                      ]
                  )
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 47,
                    width: 150,
                    child: MaterialButton(onPressed:(){
                     setState(() {
                       try{
                         bdController.updatePersonnel(
                             code_empreinte: widget.personne.code_empreinte,
                             matricule:widget.personne.matricule,
                             nom:controllerTextNom.text,
                             prenom: controllerTextPrenom.text,
                             contact: controllerTextContact.text,
                             departement: controllerTextDepartement.text,
                             titre: controllerTextTitre.text, email: controllerTextEmail.text, admin: switchValue);

                        //  if(!widget.personne.admin!&&switchValue){
                        //    bdController.addAdminUser(personne: widget.personne, context: context);
                        //  }

                         bdController.initialisation();
                         Navigator.of(context).pop();
                         ScaffoldMessenger.of(context).showSnackBar(showSnackBar("Succès","Personne modifiée avec succès\nVeuillez actualiser",Colors.green));
                         bdController.getPersonAdmin();

                       }on Exception catch(e){
                         ScaffoldMessenger.of(context).showSnackBar(showSnackBar("Erreur","${e.toString()}",Colors.red));
                       }
                     });
                    },
                      elevation:5,
                      shape:RoundedRectangleBorder(),
                      color: Color(0xFF719F9D),
                      child: Text("Modifier",style: TextStyle(fontSize: 17,color: Colors.white),),),
                  ),
                  SizedBox(width: 15,),
                  Container(
                    height: 47,
                    width: 150,
                    child: MaterialButton(onPressed:(){
                      Navigator.of(context).pop();
                    },
                      elevation:5,
                      shape:RoundedRectangleBorder(),
                      color: Color(0xFF719F9D),
                      child: Text("Annuler",style: TextStyle(fontSize: 17,color: Colors.white),),),
                  )
                ],
              )
            ],
          );
        });
  }
  Future<void> delete() async {
    showDialog(
        context: context,
        builder:(context){
          return AlertDialog(
            title:Text("Suppresion de personnes"),
            contentPadding:EdgeInsets.all(10),
            content:Container(padding: EdgeInsets.all(10),height:90,width:300,child: Center(child: Text("Voulez-vous réellement supprimer?",textAlign: TextAlign.center,style: TextStyle(fontSize: 20,color: Colors.black),))),
            actions: [
              OutlinedButton(
                  onPressed:(){
                    setState(() {
                      try{
                        bdController.deletePersonnel(matricule: widget.personne.matricule);
                        controllerDataBase.initialisation();
                        Navigator.pop(context);
                        ScaffoldMessenger.of(context).showSnackBar(showSnackBar("Succès","Suppresion effectuée avec succès\nVeuillé actualiser",Colors.green));

                      }on Exception catch(e){
                        ScaffoldMessenger.of(context).showSnackBar(showSnackBar("Erreur","${e.toString()}",Colors.red));
                      }
                    });
                  },
                  child:Text("Oui")
              ),
              SizedBox(width: 100,),
              OutlinedButton(
                  onPressed:(){
                    Navigator.of(context).pop();
                  },
                  child:Text("Non")
              ),
            ],
          );
        });
  }
}