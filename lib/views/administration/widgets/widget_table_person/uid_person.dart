import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:typed_data';
import 'package:serial_port_win32/serial_port_win32.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:up_pro/controller/controllerDataBase.dart';
import 'package:up_pro/models/personnel.dart';
import 'package:lottie/lottie.dart';

class UidPersonn extends StatefulWidget {
  final PersonnelModel personne;

  const UidPersonn({super.key, required this.personne, });

  @override
  State<UidPersonn> createState() => _UidPersonnState();
}

class _UidPersonnState extends State<UidPersonn> {

  List<String> PortSerials=[];
  String Port="";
  late SerialPort PortChecked;
  List<String> datas=[];
  late TextEditingController writingEditingController;
  final ControllerDataBase bdController=ControllerDataBase();
  bool ready=false;
  String data = '';
  @override
  void initState() {
    PortSerials=SerialPort.getAvailablePorts();
    super.initState();
  }

  void getUidData(){
      final List<PortInfo> portInfoLists = SerialPort.getPortsWithFullMessages();

      if (PortSerials.isNotEmpty) {
        PortChecked = SerialPort(Port,
            openNow: false, ReadIntervalTimeout: 1, ReadTotalTimeoutConstant: 2);
        PortChecked.open();
        print(PortChecked.isOpened);
        PortChecked.readBytesOnListen(16, (value) {
          data = String.fromCharCodes(value);
          print(DateTime.now());
          print(data);
          setState(() {
            data = String.fromCharCodes(value);
          });
        });
    }
  }


  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children:[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("Nom: ${widget.personne.nom}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17),),
                    SizedBox(width:5,),
                    Text("Prenom: ${widget.personne.prenom}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17),),

                  ],
                ),
                SizedBox(height:10,),
                Container(
                  width: 300,
                  child: DropdownSearch(
                    items:PortSerials,
                    onChanged: (index){
                      setState(() {
                        Port=index!;
                      });
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: 300,
                  child: DropdownSearch(
                    selectedItem: "115200",
                    enabled: false,
                  ),
                ),
                SizedBox(height:10,),
                Center(
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.blueAccent,
                        minimumSize: Size(300, 50),
                      ),
                      onPressed:(){
                         setState(() {
                           getUidData();
                         });
                      },
                      child: Text("Commencer Lecture",style: TextStyle(fontSize: 17,color: Colors.white),)
                  ),
                ),
                Container(
                  height:150,
                  width:300,
                    decoration:BoxDecoration(
                        boxShadow: [
                          BoxShadow(offset: Offset(-2,2),color: Colors.black12),
                        ]
                    ),
                  margin: EdgeInsets.only(top:10,left:15,bottom:10),
                  child:Center(child:Text(data,style: TextStyle(fontWeight: FontWeight.bold,fontSize:18,color:Colors.black),))
                ),
                SizedBox(height:10,),
                OverflowBar(
                  children: [
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.blueAccent,
                          minimumSize: Size(170, 50),
                        ),
                        onPressed:(){
                          setState(() {
                            ready=false;
                            PortChecked.close();
                            try{
                              bdController.updatePersonnel(
                                  matricule:widget.personne.matricule,
                                  code_empreinte: data.replaceAll(RegExp(r'[\s\n\r]'), ''),
                                  nom:widget.personne.nom,
                                  prenom: widget.personne.prenom,
                                  contact: widget.personne.contact,
                                  departement: widget.personne.departement,
                                  titre: widget.personne.titre,
                                  email: widget.personne.email,
                                  admin: widget.personne.admin
                              );

                            }on Exception catch(e){

                            }
                          });
                        },
                        child: Text("Confirmer le code",style: TextStyle(fontSize: 17,color: Colors.white),)
                    ),
                    SizedBox(width:10,),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.blueAccent,
                          minimumSize: Size(90, 50),
                        ),
                        onPressed:(){
                          setState(() {
                            Navigator.of(context).pop();
                            ready=false;
                            PortChecked.close();
                          });
                        },
                        child: Text("Annuler",style: TextStyle(fontSize: 17,color: Colors.white),)
                    ),
                  ],
                ),
                SizedBox(height:5,),
              ]
          ),
        ),
      ],
    );
  }
}
