import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:stepper_a/stepper_a.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:up_pro/controller/controllerDataBase.dart';
import 'package:vs_scrollbar/vs_scrollbar.dart';

import '../../../constants/color_global.dart';
import '../../../models/personnel.dart';
import 'widget_table_person/table_gestion_administration_row.dart';
import 'widget_table_person/table_gestion_persone_entete.dart';

class GestionAdministration extends StatefulWidget {
  const GestionAdministration({super.key});

  @override
  State<GestionAdministration> createState() => _GestionAdministrationState();
}

class _GestionAdministrationState extends State<GestionAdministration> {
  ScrollController scrollController=ScrollController();
  late ControllerDataBase bdController;
  final StepperAController controller = StepperAController();
  final supabase = Supabase.instance.client;
  bool stepIsActive=false;
  String? email;
  List<PersonnelModel> filtered=[];
  List<dynamic>listUserPerson=[];
  GlobalKey<FormState> formKeyUser=GlobalKey<FormState>();


  @override
  void initState() {
    bdController =Get.find();
    bdController.initialisation();
    email= supabase.auth.currentUser?.email;
    filtered=bdController.listePersonnAdmin;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.all(10),
          height: 600,
          width: double.infinity,
          child: Column(
            children: [
              Center(
                child: Row(
                  children: [
                    Container(
                      height: 50,
                      width: 600,
                      child: TextFormField(
                        decoration: InputDecoration(
                            fillColor: Colors.black12,
                            filled: true,
                            hintText: "Rechercher Personne",
                            hintStyle: const TextStyle(fontSize: 15),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide:BorderSide(color:colorBlueGlobal)
                            )
                        ),
                        onChanged: (value){
                          setState(() {
                            filtered=bdController.listePersonnAdmin.where((personne){
                              final personneNomPrenom="${personne.nom.toLowerCase()} ${personne.prenom.toLowerCase()}";
                              return personne.nom.toLowerCase().contains(value.toLowerCase())||
                                  personne.prenom.toLowerCase().contains(value.toLowerCase())||
                                  personneNomPrenom.toLowerCase().contains(value.toLowerCase());
                            }
                            ).toList();
                          });
                        },
                      ),
                    ),
                    const Spacer(),
                    Padding(
                      padding: const EdgeInsets.only(right: 17.0),
                      child: IconButton(
                        splashRadius:20,
                        tooltip: "Actualiser",
                        onPressed: (){
                          setState(() {
                            bdController.initialisation();
                          });
                        },
                        mouseCursor: SystemMouseCursors.click,
                        icon: const Icon(Icons.refresh,size: 33,color: Colors.black,),
                      ),
                    )
                  ],
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                physics: const BouncingScrollPhysics(),
                child: Container(
                  margin: const EdgeInsets.only(top:10),
                  height:500,
                  width: 1200,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const TableauHeader(),
                      Container(
                        margin: const EdgeInsets.only(top:3),
                        height:420,
                        width: 1100,
                        child: VsScrollbar(
                          isAlwaysShown: true,
                          controller: scrollController,
                          child: Obx(()
                          => filtered.length>0?ListView.builder(
                              controller: scrollController,
                              itemCount: filtered.length,
                              itemBuilder:(context,index){
                                return RowTableGestionPersonAdmin(personne: filtered[index],email:email);
                              }):Center(child: Container(
                              height: 60,
                              padding: const EdgeInsets.all(12),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(40)
                              ),
                              child: const Text("Aucun enregistrment",style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20
                              ),)),
                          ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}

