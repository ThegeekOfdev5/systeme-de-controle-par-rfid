import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:go_router/go_router.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:up_pro/controller/controllerDataBase.dart';
import 'package:up_pro/models/personnel.dart';

import '../../../constants/color_global.dart';
import '../../widgets/showsnackbar.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> FormKey=GlobalKey<FormState>();
  bool _obsureText = true;
  bool isLoadedPage=false;
  final RegExp regex = RegExp(r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$');
  late ControllerDataBase _controllerDB;
  late final TextEditingController _emailController;
  late final TextEditingController _passwordController;
  final supabase = Supabase.instance.client;
  final  storage=FlutterSecureStorage();

  @override
  void initState() {
    Get.put(ControllerDataBase());
    _controllerDB=Get.find();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    super.initState();
  }
  void login(BuildContext context) async {
    setState(() {
      isLoadedPage = true;
    });
    try {
      final result = await supabase.auth.signInWithPassword(
        email: _emailController.text,
        password: _passwordController.text,
      );

      final email = result.user?.email;
      final acces_token = result.session?.accessToken;
      List<dynamic>personad=await supabase.from("Personnel").select().eq("Email", _emailController.text);
      _controllerDB.personneAdmin.value=personad.map((e) => PersonnelModel.fromJson(e)).toList();


      if (email !=null && acces_token !=null) {
        await storage.write(key: 'logged', value: "true");
        await storage.write(key: 'email', value: email);
        await Future.delayed(Duration(milliseconds: 500));
        ScaffoldMessenger.of(context).showSnackBar(showSnackBar("Succès","Connecté avec succès",Colors.green));
        context.go("/home");
        setState(() {
          isLoadedPage = false;
        });
      }
      else {
        final message = "Vos identifiants sont incorrectes";
        await Future.delayed(Duration(seconds: 2));
        setState(() {
          isLoadedPage = false;
        });
        ScaffoldMessenger.of(context).showSnackBar(showSnackBar("Echec",message,Colors.red));
      }
    } on Exception catch (e) {
      final message = "Un problème est survenu de type ${e.toString()}";
      print(e);
      await Future.delayed(Duration(seconds: 1));
      setState(() {
        isLoadedPage = false;
      });
      ScaffoldMessenger.of(context).showSnackBar(showSnackBar("Echec",message,Colors.red));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        decoration: BoxDecoration(
          color:colorBlueGlobal,
        ),
        child: Center(
          child:Stack(
            children: [
              ClipPath(
                clipper: RoundedDiagonalPathClipper(),
                child: Container(
                  height: 450,
                  width: 800,
                  decoration: BoxDecoration(
                    color: Color(0xFFFEBA33),
                  ),
                  child:Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: SizedBox(
                          height:450,
                          width: 300,
                          child: Image.asset("assets/images/loginImage.jpg",fit: BoxFit.cover,),
                        ),
                      ),
                      Expanded(child: Container(child:Form(
                        key: FormKey,
                        child: Center(
                          child: SizedBox(
                            height: 400,
                            width: 380,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 20,),
                                RichText(text: TextSpan(text:"Se Connecter à ",style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: Color(0xFF0D0C22),
                                ),
                                    children: [
                                      TextSpan(text:"Labo-Management",style:TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        color: Color(0xFF0D0C22),
                                      ))
                                    ]
                                )),
                                SizedBox(
                                  height: 20,
                                ),
                                TextFormField(
                                  controller: _emailController,
                                  decoration: InputDecoration(
                                      fillColor: Colors.white,
                                      filled: true,
                                      hintText: "Email..",
                                      prefixIcon: Icon(Icons.mail_lock_outlined),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10)
                                      )
                                  ),
                                  validator: (value) {
                                    if (value == null ||
                                        value.isEmpty ||
                                        !GetUtils.isEmail(
                                            value)) {
                                      return 'Svp veuillez entrer un e-mail correct.';
                                    }
                                    return null;
                                  },
                                ),
                                SizedBox(height: 7,),
                                TextFormField(
                                  controller: _passwordController,
                                  obscureText: _obsureText,
                                  decoration: InputDecoration(
                                      fillColor: Colors.white,
                                      filled: true,
                                      hintText: "Password..",
                                      prefixIcon: Icon(Icons.security_sharp),
                                      suffixIcon:GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            _obsureText = !_obsureText;
                                          });
                                        },
                                        child: Icon(_obsureText
                                            ? Icons.visibility
                                            : Icons.visibility_off),
                                      ),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10)
                                      )
                                  ),
                                  validator: (value) {
                                    if (value == null ||
                                        value.isEmpty ||
                                        !regex.hasMatch(value)) {
                                      return 'Le mot de passe doit avoir au moins de 8 caractères.';
                                    }
                                    return null;
                                  },
                                ),
                                SizedBox(height: 20,),
                                SizedBox(
                                  width: double.infinity,
                                  height: 50,
                                  child: MaterialButton(
                                    onPressed:(){
                                      if(FormKey.currentState!.validate()){
                                          login(context);
                                      }
                                    },
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8),
                                        side:BorderSide(width: 1,color: Colors.black)
                                    ),
                                    child: isLoadedPage?SpinKitWave(color: Colors.black,size: 20,):
                                    Text("Se Connecter..",style:TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white)
                                    ),
                                    color:colorBlueGlobal,

                                  ),
                                ),
                                SizedBox(height: 5,),
                                Divider(color: Colors.black12,),
                                SizedBox(height: 5,),
                                SizedBox(
                                  width: double.infinity,
                                  height: 40,
                                  child: TextButton(
                                    onPressed: null,
                                    style: TextButton.styleFrom(

                                    ),
                                    child: Text("Labo-manage'organisation",style:TextStyle(
                                        fontSize: 10,
                                        fontStyle: FontStyle.italic,
                                        fontWeight: FontWeight.bold,
                                        color:colorBlueGlobal)),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )))
                    ],
                  ),
                ),
              ),
              Positioned(
                  top: 80,
                  left: 30,
                  child:Text("Labo-Manage..",style:TextStyle(
                      fontSize: 15,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey)))
            ],
          )
        ),
      )
    );
  }
}
