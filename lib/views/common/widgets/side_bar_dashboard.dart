import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:go_router/go_router.dart';
import 'package:quds_popup_menu/quds_popup_menu.dart';
import 'package:sidebarx/sidebarx.dart';
import 'package:just_the_tooltip/just_the_tooltip.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:up_pro/controller/controllerDataBase.dart';
import '../../../constants/color_global.dart';

class SideBarDashboard extends StatefulWidget {
  final SidebarXController controller;
  const SideBarDashboard({super.key, required this.controller});

  @override
  State<SideBarDashboard> createState() => _SideBarDashboardState();
}

class _SideBarDashboardState extends State<SideBarDashboard> {

  final storage = FlutterSecureStorage();
  final ControllerDataBase _controllerDB =Get.find();
  final supabase = Supabase.instance.client;
  String email="";
  String nom="";
  String prenom="";
  List<Map<String,dynamic>>ItemsDrawer=[
    {
      "Num":1,
      "Icon":Icons.home_outlined,
      "Title":"Accueil"
    },
    {
      "Num":2,
      "Icon":Icons.add_moderator_outlined,
      "Title":"Gestion"
    },
    {
      "Num":3,
      "Icon":Icons.person_remove,
      "Title":"Profil"
    },
    {
      "Num":4,
      "Icon":Icons.keyboard_command_key_outlined,
      "Title":"Historique"
    },
  ];

  // void init()async{
  //   String? email=await storage.read(key:"email");
  //   List<dynamic>personad=await supabase.from("Personnel").select().eq("Email",email);
  //   _controllerDB.personneAdmin.value=personad.map((e) => PersonnelModel.fromJson(e)).toList();
  // }
  //
  // @override
  // void initState() {
  //   init();
  //   _controllerDB.getPersonAdmin();
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    final widthDash=MediaQuery.of(context).size.width;
    return SidebarX(
      controller: widget.controller,
      theme: SidebarXTheme(
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: canvasColor,
          borderRadius: BorderRadius.circular(20),
        ),
        hoverTextStyle:TextStyle(fontSize: 13,fontWeight: FontWeight.bold,color: Colors.white),
        textStyle: TextStyle(fontSize: 13,fontWeight: FontWeight.bold,color: Colors.white.withOpacity(0.7)),
        selectedTextStyle: TextStyle(fontSize: 16,fontWeight: FontWeight.bold,color: Colors.white),
        itemTextPadding: const EdgeInsets.only(left: 30),
        selectedItemTextPadding: const EdgeInsets.only(left: 30),
        itemDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: canvasColor),
        ),
        selectedItemDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: actionColor.withOpacity(0.37),
          ),
          gradient: LinearGradient(
            colors: [accentCanvasColor, canvasColor],
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.28),
              blurRadius: 30,
            )
          ],
        ),
        iconTheme: IconThemeData(
          color: Colors.white.withOpacity(0.7),
          size: 20,
        ),
        selectedIconTheme: const IconThemeData(
          color: Colors.white,
          size: 30,
        ),
      ),
      extendedTheme: SidebarXTheme(
        width: 200,
        decoration: BoxDecoration(
          color: canvasColor,
        ),
      ),
      footerDivider: divider,
      headerBuilder: (context, extended) {
        return Padding(
          padding: const EdgeInsets.only(top:12.0,bottom: 40),
          child: QudsPopupButton(
            backgroundColor:Color(0xFFF8F9FB),
            items: getMenuItems(shortName: "${_controllerDB.personneAdmin[0].prenom[0]}${_controllerDB.personneAdmin[0].nom[0]}",name: "${_controllerDB.personneAdmin[0].prenom} ${_controllerDB.personneAdmin[0].nom}",email:_controllerDB.personneAdmin[0].email,ctx:context),
            child: CircleAvatar(radius: 23,backgroundColor: Colors.white,
              child:Text("${_controllerDB.personneAdmin[0].prenom[0]}${_controllerDB.personneAdmin[0].nom[0]}",style:TextStyle(fontSize:25,fontWeight: FontWeight.bold,color:colorBlueGlobal)),),
          ),
        );
      },
      footerBuilder:(context,extended){
        return Padding(
          padding: const EdgeInsets.only(bottom: 5,top:10),
          child: InkWell(mouseCursor: SystemMouseCursors.click,onTap: (){
            setState(() {
              context.go("/settings");
            });
          },
              child: JustTheTooltip(
                  content:Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Paramètres",style:TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.white)),
                  ),
                  preferredDirection: AxisDirection.right,
                  backgroundColor: Colors.black,child: Icon(Icons.settings,size: 30,color: Colors.white,))),
        );
      },
      items: ItemsDrawer.map((item) => SidebarXItem(
        icon: item["Icon"],
        label: item["Title"],
        onTap: (){
          setState(() {
            item["Num"]==1?context.go("/home"):
            item["Num"]==2?context.go("/admin"):
            item["Num"]==3?context.go("/profil"):
            item["Num"]==4?context.go("/historique"):context.go("/home");
          });
        }
      )).toList(),
    );
  }
  List<QudsPopupMenuBase> getMenuItems({required String shortName,required String name,required String email,required ctx}) {
    return [
      QudsPopupMenuItem(
          leading: CircleAvatar(
            backgroundColor: Colors.white,
            child: Center(
                child: Text(shortName,style:TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color:colorBlueGlobal))
            ),
          ),
          title: Text(
            "${name}",
            style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
          ),
          subTitle: Text('${email}'),
          onPressed: () {}),
      QudsPopupMenuDivider(),
      QudsPopupMenuWidget(
          builder: (context) => Container(
              padding: const EdgeInsets.all(10),
              width: 200,
              child: IntrinsicHeight(
                child: ElevatedButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: const Text('Voulez-vous quitter cette application ?'),
                            content: const SizedBox(width:200,child: Text('Cliquez sur Oui pour vous déconnecter.')),
                            actionsAlignment: MainAxisAlignment.spaceBetween,
                            actions: <Widget>[
                              OutlinedButton(
                                onPressed: () {
                                  setState(() {
                                    Navigator.pop(context);
                                  });
                                },
                                child: Text('Non'),
                              ),
                              OutlinedButton(
                                onPressed: () async{
                                  setState(()async{
                                    await storage.write(key: 'logged', value: "");
                                    await storage.write(key: 'email', value: "");
                                    supabase.auth.refreshSession();
                                    await storage.deleteAll();
                                    Navigator.pop(context);
                                    GoRouter.of(ctx).go("/login");

                                  });
                                },
                                child: Text('Oui'),
                              ),
                            ],
                          );
                        },);
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor:colorBlueGlobal.withOpacity(0.8),
                      shape: const StadiumBorder(),
                    ),
                    child: Container(
                        alignment: Alignment.center,
                        width: double.maxFinite,
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Text("Se deconnecter",style:TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.white))
                    )),
              )))
    ];
  }

}
final primaryColor = colorBlueGlobal.withOpacity(0.5);
Color canvasColor = colorBlueGlobal;//Color(0xFF6F6AF8);
dynamic accentCanvasColor = colorBlueGlobal.withOpacity(0.7);
const white = Colors.white;
final actionColor = colorBlueGlobal.withOpacity(0.5);
final divider = Divider(color: white.withOpacity(0.3), height: 1);
