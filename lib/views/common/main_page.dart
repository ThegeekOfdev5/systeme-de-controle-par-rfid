import 'package:connection_notifier/connection_notifier.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:sidebarx/sidebarx.dart';
import 'package:up_pro/views/common/page_no_connexion.dart';
import '../../controller/controllerDataBase.dart';
import 'widgets/side_bar_dashboard.dart';


class MainPage extends StatefulWidget {
  final Widget child;
  const MainPage({super.key, required this.child});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final _controller = SidebarXController(selectedIndex: 0, extended: true);
  final ControllerDataBase _controllerDB =Get.find();

  @override
  void initState() {
    Get.put(ControllerDataBase());
    _controllerDB.getPersonAdmin();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:ConnectionNotifierToggler(
          connected: Container(
        key:UniqueKey(),
        color: Color(0xFFF8F9FB),
        child: Row(
          children: [
            SideBarDashboard(controller: _controller,),//SideMenuDashboard(),
            Expanded(child:widget.child),
          ],
        ),
      ),
          disconnected: PageNoConnected(key: UniqueKey(),)
        ),
    );
  }
}
