import 'package:flutter/material.dart';

showSnackBar(String title, String message, Color backgroundColor) {
  return SnackBar(
    backgroundColor: backgroundColor,
    behavior: SnackBarBehavior.floating,
    content: Text(title),
    action: SnackBarAction(
      label: message,
      textColor: Colors.white,
      onPressed: () {
      },
    ),
  );
}

