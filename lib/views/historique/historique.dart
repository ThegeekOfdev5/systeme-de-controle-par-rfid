import 'package:date_picker_plus/date_picker_plus.dart';
import 'package:easy_container/easy_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:responsive_grid_list/responsive_grid_list.dart';
import 'package:up_pro/models/etat_pointage.dart';
import 'package:vs_scrollbar/vs_scrollbar.dart';
import '../../constants/color_global.dart';
import '../../controller/controllerDataBase.dart';
import '../../models/personnel.dart';
import '../widgets/items_list_person.dart';
import 'widgets/history_item_person.dart';

class Historique extends StatefulWidget {
  const Historique({super.key});

  @override
  State<Historique> createState() => _HistoriqueState();
}

class _HistoriqueState extends State<Historique> {
  final ScrollController _controller = ScrollController();
  DateTime date = DateTime.now();
  late ControllerDataBase bdController;
  List<PersonnelModel> filterdHistory=[];
  final TextEditingController _searchController=TextEditingController();


  @override
  void initState() {
    bdController=Get.find();
    filterdHistory=bdController.historyUser;
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 10, left: 15, bottom: 10, right: 10),
        padding: EdgeInsets.all(15),
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(15)),
        child: Container(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Container(
                      height: 50,
                      width: 600,
                      child: TextFormField(
                        controller: _searchController,
                        onChanged:(value){
                          setState(() {
                            filterdHistory=bdController.historyUser.where((personne){
                              final personneNomPrenom="${personne.nom.toLowerCase()} ${personne.prenom.toLowerCase()}";
                              return personne.nom.toLowerCase().contains(value.toLowerCase())||
                                  personne.prenom.toLowerCase().contains(value.toLowerCase())||
                                  personneNomPrenom.toLowerCase().contains(value.toLowerCase());
                            }
                            ).toList();
                                  });
                        },
                        decoration: InputDecoration(
                            fillColor: Colors.black12,
                            filled: true,
                            hintText: "Rechercher Personne",
                            hintStyle: TextStyle(fontSize: 15),
                            border: UnderlineInputBorder(),
                        ),
                      ),
                    ),
                    Spacer(),
                    Container(
                      padding: EdgeInsets.all(5),
                      height: 40,
                      width: 120,
                      color: Colors.black12,
                      child: Text(
                        bdController.historyDate.value==""?
                        "${date.day}/${date.month}/${date.year}":bdController.historyDate.value,
                        style: TextStyle(fontSize: 20, color: Colors.black),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    EasyContainer(
                        onTap: () async{
                          DateTime? picked = await showDatePickerDialog(
                            context: context,
                            initialDate: date,
                            maxDate: DateTime(DateTime.now().year+1),
                            minDate: DateTime(2023),
                          );

                          if (picked != null && picked != date) {
                            setState(() {
                              date = picked;
                              bdController.historyDate.value="${date.day}/${date.month}/${date.year}";
                              bdController.HistorySearch(date: date);
                              filterdHistory=bdController.historyUser;
                            });
                          }
                        },
                        color: Colors.black12,
                        child: Icon(
                          Icons.segment_outlined,
                          size: 25,
                          color: Colors.black,
                        ))
                  ],
                ),
              ),
              Expanded(
                  child: Container(
                      height: 700,
                      width: double.infinity,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: colorBlueGlobal,
                          borderRadius: BorderRadius.circular(20)
                      ),
                  child:Obx(()=>filterdHistory.length>0?VsScrollbar(
                    controller:_controller,
                    isAlwaysShown:true,
                    child: ListView.builder(
                      controller:_controller,
                      itemCount:filterdHistory.length,
                      itemBuilder:(context,index)=>Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child:ItemPersonHistory(personnel: filterdHistory[index], listeEtat:bdController.historyEtat)),
                    ),
                  ):
                  Center(child: Container(
                    height: 60,
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(40)
                    ),
                    child: Text("Aucun enregistrment ne corresponds à cette date",style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20
                    ),),
                  )))
                  )
              )
            ],
          ),
        ));
  }
}
