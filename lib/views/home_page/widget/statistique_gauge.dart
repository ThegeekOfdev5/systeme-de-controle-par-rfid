/// Package imports
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
/// Render the radial bar customization.
class RadialGaugeStat extends StatefulWidget {
  final double nbrPresence;
  final double nbrTotalPerosnnel;
  const RadialGaugeStat({super.key, required this.nbrPresence, required this.nbrTotalPerosnnel});

  @override
  State<RadialGaugeStat> createState() => _RadialGaugeStatState();
}

class _RadialGaugeStatState extends State<RadialGaugeStat> {
  @override
  Widget build(BuildContext context) {
    return _buildDistanceTrackerExample();
  }

  SfRadialGauge _buildDistanceTrackerExample() {
    return SfRadialGauge(
      enableLoadingAnimation: true,
      axes: <RadialAxis>[
        RadialAxis(
            showLabels: true,
            showTicks: true,
            radiusFactor:0.8,
            maximum: widget.nbrTotalPerosnnel,
            axisLineStyle: const AxisLineStyle(
                cornerStyle: CornerStyle.startCurve, thickness: 5),
            annotations: <GaugeAnnotation>[
              GaugeAnnotation(
                  angle: 90,
                  widget: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text('${widget.nbrPresence.toInt()}',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                              fontSize:30)),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 2, 0, 0),
                        child: Text(
                          '/${widget.nbrTotalPerosnnel.toInt()}',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                              fontSize:14),
                        ),
                      )
                    ],
                  )),
              GaugeAnnotation(
                angle: 124,
                positionFactor: 1.1,
                widget:
                Text('0',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Colors.black),),
              ),
              GaugeAnnotation(
                angle: 54,
                positionFactor: 1.1,
                widget: Text('${widget.nbrTotalPerosnnel.toInt()}',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Colors.black),),
              ),
            ],
            pointers: <GaugePointer>[
               RangePointer(
                value:widget.nbrPresence,
                width: 18,
                pointerOffset: -6,
                cornerStyle: CornerStyle.bothCurve,
                color: Color(0xFFF67280),
                gradient: SweepGradient(
                    colors: <Color>[Color(0xFFFF7676), Color(0xFFF54EA2)],
                    stops: <double>[0.25, 0.75]),
              ),
              MarkerPointer(
                value:widget.nbrPresence,
                color: Colors.white,
                markerType: MarkerType.circle,
              ),
            ]),
      ],
    );
  }
}
