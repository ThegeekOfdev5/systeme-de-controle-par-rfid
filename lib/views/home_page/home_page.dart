
import 'package:easy_container/easy_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart' as f_webRTC;
import 'package:get/get.dart';
import 'package:go_router/go_router.dart';
import 'package:lottie/lottie.dart';
import 'package:up_pro/controller/controllerDataBase.dart';
import 'package:up_pro/models/etat_pointage.dart';
import 'package:vs_scrollbar/vs_scrollbar.dart';
import 'package:one_clock/one_clock.dart';
import '../../constants/color_global.dart';
import '../../models/personnel.dart';
import '../widgets/items_list_person.dart';
import 'widget/statistique_gauge.dart';
import 'widget/widgetSuivieZone.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  late ControllerDataBase bdController;
  final ScrollController _controller=ScrollController();
  final _localRenderer = new f_webRTC.RTCVideoRenderer();
  late f_webRTC.MediaStream _localStream;

  @override
  void initState() {
    Get.put(ControllerDataBase());
    bdController=Get.find();
    bdController.initialisation();
    initRenderers();
    _getUserMedia();
    super.initState();
  }


  @override
  dispose() {
    _localStream.dispose();
    _localRenderer.dispose();
    super.dispose();
  }
  initRenderers() async {
    await _localRenderer.initialize();
  }
  _getUserMedia() async {
    final Map<String, dynamic> mediaConstraints = {
      'audio': false,
      'video': {
        'mandatory': {
          'minWidth':
          '1280', // Provide your own width, height and frame rate here
          'minHeight': '720',
          'minFrameRate': '30',
        },
        'facingMode': 'user',
        'optional': [],
      },
    };

    _localStream = await f_webRTC.navigator.mediaDevices.getUserMedia(mediaConstraints);

    _localRenderer.srcObject = _localStream;
  }


  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.only(top:10,left:15,bottom: 10,right: 10),
      padding: EdgeInsets.all(15),
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15)
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                widgetVisualisation(),
                Padding(
                  padding: EdgeInsets.symmetric(vertical:4,horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text("Liste des Personnes Présentent",style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.black
                      ),),
                      Spacer(),
                      IconButton(
                        splashRadius:20,
                        tooltip: "Actualiser",
                        onPressed: (){
                          setState(() {
                            bdController.initialisation();
                          });
                        },
                        mouseCursor: SystemMouseCursors.click,
                          icon: Icon(Icons.refresh,size: 33,color: Colors.black,),
                      ),
                      IconButton(
                        splashRadius:20,
                        tooltip: "voir plus",
                        onPressed: (){
                          setState(() {
                            context.go("/listItems");
                          });
                        },
                        mouseCursor: SystemMouseCursors.click,
                        icon: Icon(Icons.menu,size: 33,color: Colors.black,),
                      )
                    ],
                  ),
                ),
                widgetListePointage(),
              ],
            ),
          ),
          SizedBox(width: 20,),
          widgetSmallStatistic(),
        ],
      ),
    );
  }
  // FONCTION SPECIFIQUE//

  //Small Statistic
  Widget widgetSmallStatistic(){

    Stream<DateTime> _clock() async* {
      Future.delayed(Duration(seconds: 1));
      DateTime now = DateTime.now();
      yield now;
    }

    return Expanded(
      flex: 1,
      child: Container(
        height: double.infinity,
        width: 310,
        decoration: BoxDecoration(
            color:colorBlueGlobal,
            borderRadius: BorderRadius.circular(20)
        ),
        child:SingleChildScrollView(
          child: Container(
            height: 700,
            width: 160,
            padding: const EdgeInsets.only(left:12.0,right: 12.0,top:3),
            child: StreamBuilder<List<dynamic>>(
                stream: bdController.getAllEtat(),
                builder: (context, snapshot) {
                  if(snapshot.connectionState==ConnectionState.waiting){
                    return Center(
                      child: Container(
                        height:50,
                        width: 50,
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }

                  if(snapshot.data==null){
                    return Center(
                      child: Container(
                        padding: EdgeInsets.all(12),
                        height:70,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: Colors.white,
                        ),
                        child: Text(
                          "Mauvaise connection Verifiez votre connection",textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold,fontSize:17),
                        ),
                      ),
                    );
                  }
                  bdController.staticZone(etats:snapshot.data as List<EtatModel>);
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height:10,),
                      Text("  Statistiques",style:
                      TextStyle(fontSize:18,color: Colors.black,fontWeight:FontWeight.bold),),
                      SizedBox(height:10,),
                      Container(
                        height:250,
                        width: double.infinity,
                        decoration:BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)
                        ),
                        child: RadialGaugeStat(nbrPresence: bdController.listPresence.length.toDouble(), nbrTotalPerosnnel:bdController.listePersonnel.length.toDouble(),),
                      ),
                      SizedBox(height:20,),
                      Text("  Zone Suivie",style:
                      TextStyle(fontSize:18,color: Colors.black,fontWeight:FontWeight.bold),),
                      SizedBox(height:10,),
                      SizedBox(
                        height: 170,
                        child: ListView(
                          children: [
                            WidgetSuivieZone(number:bdController.listPresence.length, title:"Personnes presentes", color:Colors.green.withOpacity(.6)),
                            SizedBox(height:10,),
                            WidgetSuivieZone(number:bdController.listePersonnel.length-bdController.listPresence.length, title:"Personnes absentes", color:Colors.yellow.withOpacity(.6))
                          ],
                        ),
                      ),
                      SizedBox(height:7,),
                      StreamBuilder(
                        stream: _clock(),
                        builder: (context, AsyncSnapshot<DateTime> snapshot) {
                          if (snapshot.connectionState == ConnectionState.waiting) {
                            return const CircularProgressIndicator(
                              color: Colors.black,
                            );
                          }
                          return AnalogClock(
                            decoration: BoxDecoration(border: Border.all(width: 2.0, color: Colors.black), color: Colors.transparent, shape: BoxShape.circle),
                            width:double.infinity,
                            height: 150,
                            isLive: true,
                            hourHandColor: Colors.white,
                            minuteHandColor: Colors.black,
                            secondHandColor: Colors.red,
                            numberColor: Colors.black87,
                            showAllNumbers: true,
                            textScaleFactor: 2,
                            showDigitalClock: true,
                            datetime: DateTime(
                                snapshot.data!.year,
                                snapshot.data!.month,
                                snapshot.data!.day,
                                snapshot.data!.hour,snapshot.data!.minute,
                                snapshot.data!.second),
                          );
                        },
                      )
                    ],
                  );
                }
            ),
          ),
        ),
      ),
    );
  }
  //small list of presence
  Widget widgetListePointage(){

    return StreamBuilder<List<dynamic>>(
        stream:bdController.getAllEtat(),
        builder:(context,AsyncSnapshot<List<dynamic>> snapshot){
          if (snapshot.connectionState==ConnectionState.waiting){
            return Expanded(child: Container(
              height: 200,
              width: 900,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: colorBlueGlobal,
                  borderRadius: BorderRadius.circular(20)
              ),
              child: Center(
                child: Container(
                  height: 50,
                  width: 50,
                  child: CircularProgressIndicator(
                    color: Colors.orange,
                  ),
                ),
              ),
            ));
          }
          if(snapshot.data==null){
            return Expanded(
              child: Container(
                height: 200,
                width: 900,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: colorBlueGlobal,
                    borderRadius: BorderRadius.circular(20)
                ),
                child:Center(
                  child: Container(
                    height:50,
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      color: Colors.white,
                    ),
                    child: Text(
                      "Mauvaise connection, Verifiez votre connection",
                      style: TextStyle(fontWeight: FontWeight.bold,fontSize:17),
                    ),
                  ),
                ),
              ),
            );
          }
          return Expanded(
            child: Container(
              height: 200,
              width: 900,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: colorBlueGlobal,
                  borderRadius: BorderRadius.circular(20)
              ),
              child:VsScrollbar(
                controller:_controller,
                isAlwaysShown:true,
                child: ListView.builder(
                    controller:_controller,
                    itemCount:bdController.listePersonnel.length>3?bdController.listePersonnel.length-(bdController.listePersonnel.length-3):bdController.listePersonnel.length,
                    itemBuilder:(context,index)=>Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: ItemPerson(personnel: bdController.listePersonnel[index], listeEtat:snapshot.data as List<EtatModel>),
                    )),
              ),
            ),
          );
        });
  }
  //small part of visualisation
  Widget widgetVisualisation(){
    return Expanded(
      child: Container(
        height: 550,
        width: 900,
        padding:EdgeInsets.all(12),
        decoration: BoxDecoration(
            color: colorBlueGlobal,
            borderRadius: BorderRadius.circular(20),
        ),
         child:ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child:f_webRTC.RTCVideoView(
            _localRenderer,
            mirror: false,
            objectFit: f_webRTC.RTCVideoViewObjectFit.RTCVideoViewObjectFitCover,
              placeholderBuilder:(BuildContext context){
              return Stack(
                children: [
                  Positioned.fill(child: Lottie.asset("assets/lotties/reload_video.json")),
                  Center(child:
                  IconButton(
                    tooltip: "Reload",
                    onPressed: (){
                      ServicesBinding.instance.reassembleApplication();
                    },
                    icon: Icon(Icons.refresh_outlined,size:30,color: Colors.white,),
                  )),
                ],
              );
              }
          ),
        )
      ),
    );
  }
}

