import 'package:flutter/material.dart';
import 'package:up_pro/models/personnel.dart';

import '../administration/widgets/widget_table_person/uid_person.dart';

class PeopleUid extends StatefulWidget {
  const PeopleUid({super.key});

  @override
  State<PeopleUid> createState() => _PeopleUidState();
}

class _PeopleUidState extends State<PeopleUid> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: UidPersonn(
          personne: PersonnelModel(matricule:"23244", nom:"Amani", prenom: "Emmanuel", contact: "contact", departement: "departement", titre: "titre", code_empreinte: "code_empreinte", email: "email"),

        )
      ),
    );
  }
}
