import 'package:easy_container/easy_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:go_router/go_router.dart';
import 'package:vs_scrollbar/vs_scrollbar.dart';
import '../../constants/color_global.dart';
import '../../controller/controllerDataBase.dart';
import '../../models/etat_pointage.dart';
import '../../models/personnel.dart';
import '../widgets/items_list_person.dart';

class ListItems extends StatefulWidget {
  // final List<Person> person;
  ListItems({super.key});

  @override
  State<ListItems> createState() => _ListItemsState();
}

final ControllerDataBase controllerDataBase = ControllerDataBase();



class _ListItemsState extends State<ListItems> {
  final ScrollController _controller = ScrollController();
  final ControllerDataBase bdController=ControllerDataBase();
  List<PersonnelModel> filtered=[];

  @override
  void initState() {
    bdController.initialisation();
    filtered=bdController.listePersonnel;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          leading: RotatedBox(
              quarterTurns: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                  onPressed: () {
                    setState(() {
                      context.go("/home");
                    });
                  },
                  icon: Icon(
                    Icons.east_outlined,
                    size: 30,
                  ),
                ),
              )),
          title: Container(
            padding: EdgeInsets.all(10),
            height: 60,
            width: double.infinity,
            child: TextFormField(
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white,
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                hintText: "Search...",
              ),
              onChanged:(value){
                setState(() {
                  filtered=bdController.listePersonnel.where((personne){
                    final personneNomPrenom="${personne.nom.toLowerCase()} ${personne.prenom.toLowerCase()}";
                    return personne.nom.toLowerCase().contains(value.toLowerCase())||
                        personne.prenom.toLowerCase().contains(value.toLowerCase())||
                        personneNomPrenom.toLowerCase().contains(value.toLowerCase());
                  }
                  ).toList();
                });
              },
            ),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 18.0),
              child: IconButton(
                splashRadius:20,
                onPressed:(){
                setState(() {
                  bdController.initialisation();
                  filtered=bdController.listePersonnel;
                });
              }
              ,mouseCursor: SystemMouseCursors.click,
                icon: Icon(Icons.refresh,size: 33,color: Colors.black,),),
            )
          ],
        ),
        body: Container(
          padding: EdgeInsets.all(14),
          width: double.infinity,
          height:double.infinity,
          child:StreamBuilder<List<dynamic>>(
              stream:bdController.getAllEtat(),
              builder:(context,AsyncSnapshot<List<dynamic>> snapshot){
                if (snapshot.connectionState==ConnectionState.waiting){
                  return Container(
                    height: 700,
                    width: 900,
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: colorBlueGlobal,
                        borderRadius: BorderRadius.circular(20)
                    ),
                    child: Center(
                      child: Container(
                        height: 50,
                        width: 50,
                        child: CircularProgressIndicator(
                          color: Colors.orange,
                        ),
                      ),
                    ),
                  );
                }
                if(snapshot.data==null){
                  return Container(
                    height: 700,
                    width: 900,
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: colorBlueGlobal,
                        borderRadius: BorderRadius.circular(20)
                    ),
                    child:Center(
                      child: Container(
                        height:50,
                        padding: EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: Colors.white,
                        ),
                        child: Text(
                          "Mauvaise connection, Verifiez votre connection",
                          style: TextStyle(fontWeight: FontWeight.bold,fontSize:17),
                        ),
                      ),
                    ),
                  );
                }
                return Container(
                  height: 700,
                  width: 900,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: colorBlueGlobal,
                      borderRadius: BorderRadius.circular(20)
                  ),
                  child: filtered.length>0? VsScrollbar(
                      controller:_controller,
                      isAlwaysShown:true,
                      child: ListView.builder(
                          controller:_controller,
                          itemCount:filtered.length,
                          itemBuilder:(context,index)=>Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: ItemPerson(personnel: filtered[index], listeEtat:snapshot.data as List<EtatModel>),
                          )),
                    ): Center(child: Container(
                    height: 60,
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(40)
                    ),
                    child: Text("Aucun enregistrment",style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20
                    ),),
                  ),
                  ),
                );
              }),
        ));
  }
}
