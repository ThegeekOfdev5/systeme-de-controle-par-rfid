import 'dart:convert';

class EtatModel {
  String matricule;
  String etat;
  DateTime times;

  EtatModel({
    required this.matricule,
    required this.etat,
    required this.times
  });

  factory EtatModel.fromRawJson(String str) => EtatModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory EtatModel.fromJson(Map<String, dynamic> json) => EtatModel(
    matricule: json["matricule"],
    etat: json["etat"],
    times: DateTime.parse(json["created_at"]),
  );

  Map<String, dynamic> toJson() => {
    "matricule": matricule,
    "etat": etat
  };
}
