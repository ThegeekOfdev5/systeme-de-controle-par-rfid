#include <Arduino.h>
#include <ESP32_Supabase.h>

#if defined(ESP8266)
#include <ESP8266WiFi.h>
#else
#include <WiFi.h>
#endif

Supabase db;

// Put your supabase URL and Anon key here...
// Because Login already implemented, there's no need to use secretrole key
String supabase_url = "https://ttrjntfzhfivtebsgeeq.supabase.co";
String anon_key = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InR0cmpudGZ6aGZpdnRlYnNnZWVxIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDA3MTA4NjgsImV4cCI6MjAxNjI4Njg2OH0._xcglVrE3Vs-xCM9nRkluX0eN7sfbZKoLFdZCT0ROX0";

// put your WiFi credentials (SSID and Password) here
const char *ssid = "WIFI INPHB";
const char *psswd = "";

// Put Supabase account credentials here
const String email = "maximin.amani21@inphb.ci";
const String password = "@AMA225ni2003";

void setup()
{
  Serial.begin(9600);

  // Connecting to Wi-Fi
  Serial.print("Connecting to WiFi");
  WiFi.begin(ssid);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(100);
    Serial.print(".");
  }
  Serial.println("Connected!");

  // Beginning Supabase Connection
  db.begin(supabase_url, anon_key);

  // Logging in with your account you made in Supabase
  db.login_email(email, password);

  // Select query with filter and order, limiting the result is mandatory here
  String read = db.from("Etat").select("*").eq("matricule", "21INP00674").order("created_at", "asc", true).limit(1).doSelect();
  Serial.println(read);

  // Reset Your Query before doing everything else
  db.urlQuery_reset();
}

void loop()
{
  delay(10);
}